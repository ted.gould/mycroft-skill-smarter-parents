# Below is the list of outside modules you'll be using in your skill.
# They might be built-in to Python, from mycroft-core or from external
# libraries.  If you use an external library, be sure to include it
# in the requirements.txt file so the library is installed properly
# when the skill gets installed later by a user.

from adapt.intent import IntentBuilder
from mycroft.skills.core import MycroftSkill, intent_handler
from mycroft.util.log import LOG

# Each skill is contained within its own class, which inherits base methods
# from the MycroftSkill class.  You extend this class as shown below.

class SmarterParentsSkill(MycroftSkill):

    # The constructor of the skill, which calls MycroftSkill's constructor
    def __init__(self):
        super(SmarterParentsSkill, self).__init__(name="SmarterParentsSkill")
        
        # Initialize working variables used within the skill.
        self.count = 0

    # Parent vs. Parent
    @intent_handler(IntentBuilder("QueryPVP").require("Query").require("Parent").require("Or").require("Parent"))
    def handle_pvp_intent(self, message):
        self.speak_dialog("tie")

    # Kid vs. Kid
    @intent_handler(IntentBuilder("QueryKVK").require("Query").require("Kid").require("Or").require("Kid"))
    def handle_kvk_intent(self, message):
        self.speak_dialog("tie")

    # Kid vs. Parent
    @intent_handler(IntentBuilder("QueryKVPpk").require("Query").require("Parent").require("Or").require("Kid"))
    def handle_kvk_pk_intent(self, message):
        self.speak_dialog("mom")

    @intent_handler(IntentBuilder("QueryKVPkp").require("Query").require("Kid").require("Or").require("Parent"))
    def handle_kvk_kp_intent(self, message):
        self.speak_dialog("mom")

# The "create_skill()" method is used to create an instance of the skill.
# Note that it's outside the class itself.
def create_skill():
    return SmarterParentsSkill()
