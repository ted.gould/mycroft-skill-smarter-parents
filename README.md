
## smarter-parents
A quick reminder to all kids that their parents are smarter

## Description 
Made as a fun hack to troll my children, hopefully to make them
want to build skills to harass me back. It just says that your
parents are always smarter. And if they got this skill on your
Mycroft, they are!

## Examples 
* "Who's smarter, Mom or Dad?"

## Credits 
* Ted Gould
